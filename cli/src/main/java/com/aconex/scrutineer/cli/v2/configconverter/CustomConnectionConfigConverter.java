package com.aconex.scrutineer.cli.v2.configconverter;

import com.aconex.scrutineer.ConnectorConfig;
import com.aconex.scrutineer.CustomConnectorConfig;

import java.util.Map;

public class CustomConnectionConfigConverter extends ConnectorConfigConverter{
    @Override
    ConnectorConfig convert(Map<String, String> props) {
        return new CustomConnectorConfig(props);
    }
}
