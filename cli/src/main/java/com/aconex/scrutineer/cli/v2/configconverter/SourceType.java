package com.aconex.scrutineer.cli.v2.configconverter;

public enum SourceType {
    jdbc,
    elasticsearch,
    http,
    custom
}
