package com.aconex.scrutineer.cli.v2.configconverter;

import com.aconex.scrutineer.ConnectorConfig;

import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public abstract class ConnectorConfigConverter {
    abstract ConnectorConfig convert(Map<String, String> props);

    protected String getRequiredProperty(Map<String, String> props, String propertyName) {
        return checkNotNull(props.get(propertyName), "Configuration for %s is required", propertyName);
    }
}
