package com.aconex.scrutineer;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public class CustomConnectorConfig extends ConnectorConfig {
    public static final String CONFIG_CUSTOM_CONNECTOR_CLASSNAME = "stream.connector.class";
    private final Map<String, String> properties;

    public CustomConnectorConfig(Map<String, String> properties) {
        this.properties = properties;
    }

    @Override
    public IdAndVersionStreamConnector createConnector(IdAndVersionFactory idAndVersionFactory) {
        String connectorClassName = checkNotNull(properties.get(CONFIG_CUSTOM_CONNECTOR_CLASSNAME),
                "%s property must be specified when source.type=custom", CONFIG_CUSTOM_CONNECTOR_CLASSNAME);
        try {
            Constructor<?> constructor = getConnectorClassConstructor(getConnectorClass(connectorClassName));
            return (IdAndVersionStreamConnector) constructor.newInstance(this, idAndVersionFactory);
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException e) {
            throw new IllegalArgumentException(
                    String.format("Failed to invoke the constructor of the connector class %s specified in the %s property",
                            connectorClassName, CONFIG_CUSTOM_CONNECTOR_CLASSNAME), e);
        }
    }

    private Class<?> getConnectorClass(String connectorClassName) {
        try {
            return Class.forName(connectorClassName);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(
                    String.format("Can not find the connector class %s specified in the %s property in classpath",
                            connectorClassName, CONFIG_CUSTOM_CONNECTOR_CLASSNAME),
                    e);
        }
    }

    private Constructor<?> getConnectorClassConstructor(Class<?> connectorClass) {
        try {
            return connectorClass.getConstructor(ConnectorConfig.class, IdAndVersionFactory.class);
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException(
                    String.format("The connector class %s specified in the %s property does not have expected " +
                                    "constructor with parameters of " +
                                    "(ConnectorConfig connectorConfig, IdAndVersionFactory idAndVersionFactory)",
                            connectorClass.getName(), CONFIG_CUSTOM_CONNECTOR_CLASSNAME),
                    e);
        }
    }
}
