package com.aconex.scrutineer.common.config;

import java.util.HashMap;
import java.util.Map;

import com.aconex.scrutineer.common.config.provider.FileConfigProvider;
import com.aconex.scrutineer.common.config.provider.SystemEnvConfigProvider;

public class ConfigValueResolverScanner {

    public Map<String, ConfigProvider> scanForConfigProviders() {
        Map<String, ConfigProvider> configProviderInstances = new HashMap<>();
        configProviderInstances.put("env", new SystemEnvConfigProvider());
        configProviderInstances.put("file", new FileConfigProvider());
        return configProviderInstances;
    }
}
