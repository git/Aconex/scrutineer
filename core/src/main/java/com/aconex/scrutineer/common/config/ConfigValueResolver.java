package com.aconex.scrutineer.common.config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * This class wraps a set of {@link ConfigProvider} instances and uses them to perform
 * transformations.
 *
 * <p>The variable pattern is of the form <code>${provider:key}</code>,
 * where the <code>provider</code> corresponds to a {@link ConfigProvider} instance, as passed to
 * {@link ConfigValueResolver#ConfigValueResolver(Map)}.  The pattern will extract a set
 * of paths (which are optional) and keys and then pass them to {@link ConfigProvider#resolve(String)} to obtain the
 * values with which to replace the variables.
 */
@SuppressWarnings("PMD.NcssMethodCount")
public class ConfigValueResolver {
    private final Map<String, ConfigProvider> configProviders;

    /**
     * Creates a ConfigValueResolver with the default pattern, of the form <code>${provider:key}</code>.
     *
     * @param configProviders a Map of provider names and {@link ConfigProvider} instances.
     */
    public ConfigValueResolver(Map<String, ConfigProvider> configProviders) {
        this.configProviders = configProviders;
    }

    /**
     * Resolve values of the given configuration data by using the {@link ConfigProvider} instances to
     * look up values to replace the variables in the pattern.
     */
    public String resolveValue(String value) {
        List<ConfigVariable> variables = ConfigVariable.getVariablesInValue(value);
        if (variables.isEmpty()) {
            return value;
        }

        List<ResolvedConfigVariable> resolvedVariables = resolveVariables(variables);
        return replaceVariables(value, resolvedVariables);
    }

    private List<ResolvedConfigVariable> resolveVariables(List<ConfigVariable> configVariables) {
        return configVariables.stream().map(
                variable -> {
                    ConfigProvider provider = this.configProviders.get(variable.getProviderName());
                    return new ResolvedConfigVariable(
                            variable,
                            (provider == null) ? null : provider.resolve(variable.getVariable()));
                }
        ).collect(Collectors.toList());
    }

    private static String replaceVariables(String value, List<ResolvedConfigVariable> resolvedVariables) {
        StringBuilder builder = new StringBuilder();
        int i = 0;
        for (ResolvedConfigVariable resolvedVar: resolvedVariables) {
            ConfigVariable variable = resolvedVar.variable();
            String replacement = resolvedVar.resolvedValue();
            builder.append(value, i, variable.matchIndexStart());
            if (replacement == null) {
                // No replacements, just use the original value
                builder.append(value);
            } else {
                builder.append(replacement);
            }
            i = variable.matchIndexEnd();
        }

        builder.append(value, i, value.length());
        return builder.toString();
    }

    private static class ResolvedConfigVariable {
        private ConfigVariable configVariable;
        private String resolvedValue;
        ResolvedConfigVariable(ConfigVariable configVariable, String resolvedValue) {
            this.configVariable = configVariable;
            this.resolvedValue = resolvedValue;
        }
        public ConfigVariable variable() {
            return configVariable;
        }
        public String resolvedValue() {
            return resolvedValue;
        }
    }

    private static final class ConfigVariable {
        // This regExp matches pattern like "${provider:key}", e.g: given test string "Password is ${env:top_secret}"
        // 1 match is found with 3 group content captured:
        // group[0]: ${env:top_secret}
        // group[1]: env
        // group[2]: top_secret
        private static final Pattern VARIABLE_PATTERN = Pattern.compile("\\$\\{([^}]*?):([^}]*?)\\}");
        private static final int GROUP_INDEX_FOR_PROVIDER_NAME = 1;
        private static final int GROUP_INDEX_FOR_VARIABLE = 2;

        private String providerName;
        private String variable;
        private int matchIndexStart;
        private int matchIndexEnd;

        static List<ConfigVariable> getVariablesInValue(String value) {
            if (value == null) {
                return Collections.emptyList();
            }

            List<ConfigVariable> configVars = new ArrayList<>();
            Matcher matcher = VARIABLE_PATTERN.matcher(value);
            while (matcher.find()) {
                configVars.add(new ConfigVariable(matcher));
            }
            return configVars;
        }

        /**
         * Capture pattern groups following format: <code>${provider:key}</code>,
         */
        private ConfigVariable(Matcher matcher) {
            this.providerName = matcher.group(GROUP_INDEX_FOR_PROVIDER_NAME);
            this.variable = matcher.group(GROUP_INDEX_FOR_VARIABLE);
            this.matchIndexStart = matcher.start();
            this.matchIndexEnd = matcher.end();
        }

        public String getProviderName() {
            return providerName;
        }

        public String getVariable() {
            return variable;
        }

        public int matchIndexStart() {
            return matchIndexStart;
        }

        public int matchIndexEnd() {
            return matchIndexEnd;
        }

        public String toString() {
            return "(" + providerName + ":" + variable + ")";
        }
    }
}
