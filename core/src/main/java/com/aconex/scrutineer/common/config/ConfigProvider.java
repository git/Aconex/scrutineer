package com.aconex.scrutineer.common.config;

public interface ConfigProvider {
    String resolve(String value);
}
