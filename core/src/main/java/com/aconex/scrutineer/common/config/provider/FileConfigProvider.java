package com.aconex.scrutineer.common.config.provider;

import static java.lang.String.format;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.aconex.scrutineer.common.config.ConfigProvider;
import org.apache.commons.lang3.tuple.Pair;

/**
 * An implementation of {@link ConfigProvider} that represents a local Properties file.
 * All property keys and values are stored as clear text. Value reference should be define
 * in format "${file:/tmp/properties.txt:fileKey}"
 *
 * <p>If a Properties file with contents
 * <pre>
 * fileKey=someValue
 * </pre>
 * resides at the path "/tmp/properties.txt", then when a configuration Map which has an entry with a key "someKey" and
 * a value "${file:/tmp/properties.txt:fileKey}" is passed to the {@link #resolve(String)} method, then the transformed
 * Map will have an entry with key "someKey" and a value "someValue".
 */
public class FileConfigProvider implements ConfigProvider {
    private static final char PATH_SEPARATOR = ':';

    @Override
    public String resolve(String valueRef) {
        Pair<String, String> filePathToKey = separateStringBy(valueRef, PATH_SEPARATOR);
        Map<String, String> properties = readPropertyFileAsMap(filePathToKey.getKey());
        return properties.get(filePathToKey.getValue());
    }

    private Pair<String, String> separateStringBy(String value, char separator) {
        int index = value.indexOf(separator);
        if(index == -1) {
            throw new IllegalArgumentException(
                    format("Reference in the 'file' provider must be in format 'path%skey'", separator));
        }
        return Pair.of(value.substring(0, index), value.substring(index + 1));
    }

    private Reader reader(String path) throws IOException {
        return Files.newBufferedReader(Paths.get(path));
    }

    private Map<String, String> readPropertyFileAsMap(String filePath) {
        try (Reader reader = reader(filePath)) {
            Properties properties = new Properties();
            properties.load(reader);
            return new HashMap(properties);
        } catch (IOException e) {
            throw new RuntimeException("Could not read properties from file " + filePath, e);
        }
    }
}
