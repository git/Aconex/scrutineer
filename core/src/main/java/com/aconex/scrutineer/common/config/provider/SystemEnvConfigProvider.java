package com.aconex.scrutineer.common.config.provider;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

import java.util.Map;

import com.aconex.scrutineer.common.config.ConfigProvider;

public class SystemEnvConfigProvider implements ConfigProvider {
    private Map<String, String> vars;

    public SystemEnvConfigProvider() {
        this.vars = System.getenv();
    }

    // used in test to inject variables
    protected SystemEnvConfigProvider(Map<String, String> vars) {
        this.vars = vars;
    }

    @Override
    public String resolve(String valueRef) {
        return checkNotNull(vars.get(valueRef), format("Fail to lookup environment variable [%s]", valueRef));
    }

}
