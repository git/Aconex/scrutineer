package com.aconex.scrutineer.common.config;

import java.util.HashMap;
import java.util.Map;

/**
 * Encapsulate {@link ConfigValueResolver}'s capabilities via simple API(s), internally it registers {@link ConfigProvider}
 * that scrutineer supports, and delegate to {@link ConfigValueResolver} to transform the original configuration
 */
public class ConfigValueResolverExtension {
    private ConfigValueResolver configResolver;

    public ConfigValueResolverExtension() {
        this(new ConfigValueResolver(new ConfigValueResolverScanner().scanForConfigProviders()));
    }

    protected ConfigValueResolverExtension(ConfigValueResolver configResolver) {
        this.configResolver = configResolver;
    }

    public Map<String, String> resolveValues(Map<String, String> originalConfigs) {
        Map<String, String> results = new HashMap<>();
        originalConfigs.forEach((key, value) ->
                results.put(key, configResolver.resolveValue(value)));
        return results;
    }

    public String resolveValue(String originalConfig) {
        return configResolver.resolveValue(originalConfig);
    }

}
