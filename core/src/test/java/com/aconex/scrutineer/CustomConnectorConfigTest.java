package com.aconex.scrutineer;

import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class CustomConnectorConfigTest {
    @Test
    public void shouldCreateConnector() {
        Map<String, String> props = new HashMap<String, String>(){{
            put(CustomConnectorConfig.CONFIG_CUSTOM_CONNECTOR_CLASSNAME, "com.aconex.scrutineer.StubConnector");
            put("my.property","hello");
        }};
        CustomConnectorConfig connectorConfig = new CustomConnectorConfig(props);
        IdAndVersionStreamConnector stubConnector = connectorConfig.createConnector(StringIdAndVersionFactory.INSTANCE);

        assertTrue(stubConnector instanceof StubConnector);
        assertEquals(connectorConfig, ((StubConnector) stubConnector).getConnectorConfig());
    }
}
class StubConnector extends AbstractIdAndVersionStreamConnector{

    public StubConnector(ConnectorConfig connectorConfig, IdAndVersionFactory idAndVersionFactory) {
        super(connectorConfig, idAndVersionFactory);
    }

    @Override
    protected Iterator<IdAndVersion> fetchFromSource() {
        return null;
    }

    @Override
    public void open() {

    }

    @Override
    public void close() throws IOException {

    }
}