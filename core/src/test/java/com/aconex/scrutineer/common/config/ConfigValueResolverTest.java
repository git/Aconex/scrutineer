package com.aconex.scrutineer.common.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ConfigValueResolverTest {

    public static final String TEST_KEY = "testKey";
    public static final String TEST_KEY_2 = "testKey2";
    public static final String TEST_RESULT = "testResult";
    public static final String TEST_RESULT_2 = "testResult2";

    private ConfigValueResolver configValueResolver;

    @Before
    public void setup() {
        Map<String, ConfigProvider> configProviders = new HashMap<>();
        configProviders.put("test", new TestConfigProvider());
        configValueResolver = new ConfigValueResolver(configProviders);
    }

    @Test
    public void testReplaceVariable() {
        String resolvedValue = configValueResolver.resolveValue("${test:testKey}");
        assertEquals(TEST_RESULT, resolvedValue);
    }

    @Test
    public void testReplaceMultipleVariablesInValue() {
        String resolvedValue = configValueResolver.resolveValue("hello, ${test:testKey}; goodbye, ${test:testKey2}!");
        assertEquals("hello, testResult; goodbye, testResult2!", resolvedValue);
    }

    @Test
    public void testNoReplacement() {
        String resolvedValue = configValueResolver.resolveValue("${test:missingKey}");
        assertEquals("${test:missingKey}", resolvedValue);
    }

    @Test
    public void testNullConfigValue() {
        String resolvedValue = configValueResolver.resolveValue(null);
        assertNull(resolvedValue);
    }

    public static class TestConfigProvider implements ConfigProvider {
        @Override
        public String resolve(String valueRef) {
            if (TEST_KEY.equals(valueRef)) {
                return TEST_RESULT;
            }
            if (TEST_KEY_2.equals(valueRef)) {
                return TEST_RESULT_2;
            }
            return null;
        }
    }
}
