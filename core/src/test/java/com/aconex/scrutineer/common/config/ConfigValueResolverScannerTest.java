package com.aconex.scrutineer.common.config;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

import java.util.Map;

import com.aconex.scrutineer.common.config.provider.FileConfigProvider;
import com.aconex.scrutineer.common.config.provider.SystemEnvConfigProvider;
import org.junit.Before;
import org.junit.Test;

public class ConfigValueResolverScannerTest {
    private ConfigValueResolverScanner scanner;

    @Before
    public void setUp() throws Exception {
        this.scanner = new ConfigValueResolverScanner();
    }

    @Test
    public void shouldIncludeStaticallyConfiguredResolvers() {
        Map<String, ConfigProvider> configProviders = scanner.scanForConfigProviders();

        assertThat(configProviders.size(), is(2));
        assertThat(configProviders.get("env"), instanceOf(SystemEnvConfigProvider.class));
        assertThat(configProviders.get("file"), instanceOf(FileConfigProvider.class));
    }
}
