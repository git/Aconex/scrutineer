package com.aconex.scrutineer.common.config;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

public class ConfigValueResolverExtensionTest {
    private ConfigValueResolverExtension configValueResolverExtension;

    @Mock
    private ConfigValueResolver mockConfigResolver;

    private String key1 = "k1";
    private String variable1 = "v1";
    private String resolvedValue1 = "resolved1";

    private String key2 = "k2";
    private String variable2 = "v2";
    private String resolvedValue2 = "resolved2";

    @Before
    public void setUp() throws Exception {
        initMocks(this);

        when(mockConfigResolver.resolveValue(variable1)).thenReturn(resolvedValue1);
        when(mockConfigResolver.resolveValue(variable2)).thenReturn(resolvedValue2);
        this.configValueResolverExtension = new ConfigValueResolverExtension(mockConfigResolver);
    }

    @Test
    public void shouldResolveValuesFromOriginalMap() {
        Map<String, String> originalConfigs =
                ImmutableMap.<String, String>builder()
                        .put(this.key1, this.variable1)
                        .put(this.key2, this.variable2)
                        .build();

        Map<String, String> output = this.configValueResolverExtension.resolveValues(originalConfigs);

        Map<String, String> expectedValueResolved =
                ImmutableMap.<String, String>builder()
                        .put(this.key1, this.resolvedValue1)
                        .put(this.key2, this.resolvedValue2)
                        .build();

        assertThat(output, equalTo(expectedValueResolved));
    }

    @Test
    public void shouldResolveSingleVariable() {
        String output = this.configValueResolverExtension.resolveValue(variable1);
        assertThat(output, is(resolvedValue1));
    }
}
