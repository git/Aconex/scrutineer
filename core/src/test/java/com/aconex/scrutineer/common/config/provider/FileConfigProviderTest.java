package com.aconex.scrutineer.common.config.provider;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.Before;
import org.junit.Test;

public class FileConfigProviderTest {
    private Path tempFile;

    private FileConfigProvider configProvider;

    @Before
    public void setup() throws IOException {
        this.tempFile = Files.createTempFile("file-config-provider", null);
        this.configProvider = new FileConfigProvider();
    }

    @Test
    public void testResolveValueFromFile() throws IOException {
        String filePath = generatePropertyFileForConfigEntry("token=h4af5hb");
        String valueRef = filePath + ":token";
        assertEquals("h4af5hb", configProvider.resolve(valueRef));
    }

    @Test
    public void keyNotFoundInFile() throws IOException {
        String filePath = generatePropertyFileForConfigEntry("token=h4af5hb");
        String valueRef = filePath + ":keyNotFound";
        assertNull(configProvider.resolve(valueRef));
    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowExceptionIfFileNotFound() {
        String valueRefWithFilePathNotFound = "fileNotFound:keyNotFound";
        configProvider.resolve(valueRefWithFilePathNotFound);
    }

    private String generatePropertyFileForConfigEntry(String content) throws IOException {
        File generatedPropertyFile = Files.write(tempFile, content.getBytes(UTF_8)).toFile();
        return generatedPropertyFile.getAbsolutePath();
    }
}
