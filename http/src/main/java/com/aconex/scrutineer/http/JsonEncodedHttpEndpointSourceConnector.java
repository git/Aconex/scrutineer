package com.aconex.scrutineer.http;

import com.aconex.scrutineer.AbstractIdAndVersionStreamConnector;
import com.aconex.scrutineer.ConnectorConfig;
import com.aconex.scrutineer.IdAndVersion;
import com.aconex.scrutineer.IdAndVersionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Iterator;

public class JsonEncodedHttpEndpointSourceConnector extends AbstractIdAndVersionStreamConnector {
    private final Logger logger = LoggerFactory.getLogger(JsonEncodedHttpEndpointSourceConnector.class);
    private HttpURLConnection httpConnection;
    private InputStream responseInputStream;

    protected JsonEncodedHttpEndpointSourceConnector(ConnectorConfig connectorConfig, IdAndVersionFactory idAndVersionFactory) {
        super(connectorConfig, idAndVersionFactory);
    }

    @Override
    public void open() {
        try {
            disableSSLValidation();
            responseInputStream = sendRequest(getConfig());
        } catch (Exception e) {
            throw new RuntimeException("Failed to list entities from source endpoint: " + getConfig(), e);
        }
    }

    public Iterator<IdAndVersion> fetchFromSource() {
        return new JsonEncodedIdAndVersionInputStreamIterator(responseInputStream, getIdAndVersionFactory());
    }

    private InputStream sendRequest(HttpConnectorConfig config) throws IOException {
        String queryUrl = config.getHttpEndpointUrl();
        logger.info("Querying http endpoint: {}", queryUrl);

        httpConnection = prepareConnection(config, queryUrl);
        httpConnection.connect();
        return new BufferedInputStream(httpConnection.getInputStream());
    }

    private HttpURLConnection prepareConnection(HttpConnectorConfig config, String queryUrl) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(queryUrl).openConnection();
        connection.setRequestMethod("GET");
        connection.setConnectTimeout(config.getHttpConnectionTimeoutInMillisecond());
        connection.setReadTimeout(config.getHttpReadTimeoutInMillisecond());
        return connection;
    }

    public void disableSSLValidation() {
        try {
            HttpsURLConnection.setDefaultSSLSocketFactory(createSslContext().getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> true);
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            logger.warn("Failed to disable SSL validation", e);
        }
    }

    private SSLContext createSslContext() throws NoSuchAlgorithmException, KeyManagementException {
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, new TrustManager[]{createDumbTrustManager()}, null);
        return sslContext;
    }

    private X509TrustManager createDumbTrustManager() {
        return new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        };
    }

    @Override
    public void close() {
        closeResponseInputStream();
        closeHttpConnection();
    }

    private void closeResponseInputStream() {
        if (responseInputStream != null) {
            try {
                responseInputStream.close();
            } catch (IOException e) {
                logger.warn("Failed to close the http response http stream", e);
            }
        }
    }

    private void closeHttpConnection() {
        if (httpConnection != null) {
            try {
                httpConnection.disconnect();
            } catch (Exception e) {
                logger.warn("Failed to disconnect http url connection", e);
            }
        }
    }

    private HttpConnectorConfig getConfig() {
        return (HttpConnectorConfig) getConnectorConfig();
    }
}
